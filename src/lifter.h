#ifndef LIFTER_H
#define LIFTER_H

#include <WPIlib.h>
#include "pidcontroller.h"

class Lifter
{
public:
  Lifter();
  void ManualControl(float lifter, bool ignoreSlowdown); // for teleop
  void update(); // actually do stuff
  void gotoTarget(float h);// be carefu
  void setClaw(bool l, bool r);
  void resetGoto();
  void Enable();
  ~Lifter();
//private:
  CANTalon lifterR;
  CANTalon lifterL;

  Solenoid rightClaw;
  Solenoid leftClaw;

  DigitalInput lifterUBound;
  DigitalInput lifterLBound;

  int lEncBound;
  int uEncBound;
  int target;

  float lifter;
  bool ignoreSlowdown;

  PIDDY PIDdy;
};

#endif // LIFTER_H
