#include "pidcontroller.h"

#include <string>

double dabs(double d);

PIDDY::PIDDY(double a, double b, double c, double d)
{
  Set(a,b,c,d);
}

void PIDDY::Set(double p, double i, double d, double iMax)
{
  this->d=d;
  this->i=i;
  this->p=p;
  this->iMax=iMax;
  this->integral=0;
  this->previousError=0;
}

double PIDDY::update(double target, double sensor)
{
  double error = target-sensor;

  if((error==0)||(dabs(error)>iMax))
    integral=0;
  else
    integral+=error;

  double derivative = error - previousError;
  previousError=error;
  return (p*error) + (i*integral) + (d*derivative);
}

PIDDY::~PIDDY()
{}
DriveTalon::DriveTalon(int id) : CANTalon(id)
{
  this->PIDdy==NULL;
  this->id = id;
}

void DriveTalon::setInfo(double p, double i, double d, double iMax, int DIOa, int DIOb)
{
  PIDdy = new PIDDY(p,i,d,iMax);
  sensor = new PWMInput(DIOa,DIOb);
}
void DriveTalon::Set(double target)
{// weird syntax here. take care when traversing.
  if(this->PIDdy==NULL) return this->CANTalon::Set(target);
  double v = PIDdy->update(target,sensor->PIDGet());
  this->CANTalon::EnableControl();
  this->CANTalon::Set(v);
  //std::string str = "_DriveTrainFR" + std::to_string(id);
  //SmartDashboard::PutNumber(str,v);
}
