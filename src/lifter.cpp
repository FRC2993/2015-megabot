#include "lifter.h"

double dabs(double d);

Lifter::Lifter():
  lifterR(30),//30-39 for lifter CAN ids
  lifterL(31),
  rightClaw(11,3), // 10-19 for system CAN ids
  leftClaw(11,4),
  lifterUBound(8),//DIO
  lifterLBound(9),
  PIDdy(0.0005415,0.000043296,0.000451,800) //Ultimate gain is 0.00164, Oscillation period is 25
{
  lEncBound=0;
  uEncBound=14000;
  target=0;

  lifter=0.0;
  ignoreSlowdown = false;

  #define SETUP_TALON( name ) name .SetExpiration(.250); name .SetSafetyEnabled(true)
  SETUP_TALON(lifterR);// safety
  SETUP_TALON(lifterL);
}

void Lifter::Enable()
{
#define ENABLE_TALON( name ) name .EnableControl(); name .Set(0)
  ENABLE_TALON(lifterR);
  ENABLE_TALON(lifterL);
  resetGoto();
}

void Lifter::resetGoto()
{
  ignoreSlowdown = false;
  target = lifterL.GetEncPosition();
}

void Lifter::ManualControl(float lifter, bool ignoreSlowdown)
{
  this->lifter=lifter;
  this->ignoreSlowdown=ignoreSlowdown;
}

void Lifter::update()
{
  double pos = lifterL.GetEncPosition();
  //std::cout << pos << " " << lifter <<std::endl;

  if(dabs(lifter)<.07)// correction
  {
    lifter = PIDdy.update(target,pos);
    float speed = lifterL.GetSpeed();// attempt to remove jitter
    if((speed<-50) && lifter>.2)
    {
      lifter=.1;
      target=pos;
    }
    if(speed>150 && lifter<-.2)
    {
      lifter=0;
      target=pos;
    }
  }
  else
  {
    target=pos;
  }

  if(lifterUBound.Get()) uEncBound = pos; // set bounds
  if(lifterLBound.Get()) lEncBound = pos;

  if(!ignoreSlowdown) // slow near bounds when going toward them
  {
    if(lifter<0 && pos<(lEncBound+1200)) {lifter/=3;std::cout <<"LBound";}
    if(lifter>0 && pos>(uEncBound-1200)) {lifter/=1.5;std::cout << "UBound";}
  }

  if(lifter<0)lifter/=2; // bounds & same speed down
  if(lifterUBound.Get()&&lifter>0) lifter = 0;
  if(lifterLBound.Get()&&lifter<0) lifter = 0;

  lifterL.EnableControl();
  lifterR.EnableControl();
  lifterR.Set(lifter);
  lifterL.Set(-lifter);
  //std::cout << lifter << std::endl;
}

void Lifter::gotoTarget(float h)
{
  target = lEncBound+((uEncBound-lEncBound)*h);
  ignoreSlowdown = false;
  lifter=0;
}
void Lifter::setClaw(bool l, bool r)
{
  rightClaw.Set(r);
  leftClaw.Set(l);
}

Lifter::~Lifter()
{
}

