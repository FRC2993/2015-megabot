#include "autonframework.h"
#include <ctime>

//---------- ADD MORE HERE
#include "auto/Auto0.hpp"
#include "auto/Auto1.hpp"
#include "auto/Auto2.hpp"
#include "auto/Auto3.hpp"
void AutoStuff::setAuton(int autonIndex)
{
  if(currentAuton!=NULL)
    delete currentAuton;
  switch(autonIndex)
  {
  case 0:
    currentAuton = new Auto0(drive,lifter);
    break;
  case 1:
    currentAuton = new Auto1(drive,lifter);
    break;
  case 2:
    currentAuton = new Auto2(drive,lifter);
    break;
  case 3:
    currentAuton = new Auto3(drive,lifter);
    break;
  // add more here
  default:
    if(autonIndex==defaultAuton) return; // what
    setAuton(defaultAuton);
    break;
  }
}


AutoStuff::AutoStuff(Drivetrain *drive, Lifter *lifter, LiveWindow *liveWindow):
  autonSelector()
{
  this->currentAuton=NULL;
  this->defaultAuton=1;
  this->drive=drive;
  this->lifter=lifter;
  this->liveWindow=liveWindow;
  autonSelector.InitTable(NetworkTable::GetTable("SmartDashboard"));
  autonSelector.AddDefault("0 Tote Auto",(void*)0);
  autonSelector.AddObject("1 Tote Auto",(void*)1);// and here
  autonSelector.AddObject("2 Tote Auto",(void*)2);// and here
  autonSelector.AddObject("3 Tote Auto",(void*)3);
  SmartDashboard::PutData("Autonomous Mode Selector",&autonSelector);
}

void AutoStuff::onPeriodic()
{
  //this->currentAuton->Periodic(difftime(time(0),start));
}
void AutoStuff::onInit()
{
  setAuton((int)autonSelector.GetSelected());
  this->currentAuton->init();
  start = time(0);
}
void AutoStuff::setDefaultAuton(int defaultAuton)
{
  this->defaultAuton=defaultAuton;
}

int AutoStuff::getDefaultAuton()
{
  return this->defaultAuton;
}

Autonomous::~Autonomous(){}

