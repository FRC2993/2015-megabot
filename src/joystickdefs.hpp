#ifndef JOYSTICKDEFS_HPP
#define JOYSTICKDEFS_HPP

#define JOY( s , v ) s -> GetRawAxis( v )
#define JOD( s , v ) s -> GetRawButton( v )

#define DriveX (-JOY( RightJoystick , 0 ))
#define DriveY (-JOY( RightJoystick , 1 ))
#define DriveR (JOY( RightJoystick , 2 ))
#define DriveIgnoreAxis ( JOD( RightJoystick , 10 ) || JOD( RightJoystick , 11 ) || JOD( RightJoystick , 12 ))
#define Throttle (1.0 - RightJoystick->GetAxis(Joystick::kThrottleAxis))

#define ClawLeftMoment JOD( LeftJoystick, 2 )
#define ClawToggle JOD( LeftJoystick, 3 )
#define ClawRightMoment JOD( LeftJoystick, 4 )
#define LiftVertical (JOY( LeftJoystick, 1 ))
#define LiftFaster ( LeftJoystick->GetPOV(0)!=-1)

#define CompressorToggle (JOD(LeftJoystick, 8 ))

extern Joystick *RightJoystick;
extern Joystick *LeftJoystick;

#endif // JOYSTICKDEFS_HPP

