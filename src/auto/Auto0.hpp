#ifndef AUTO_0_GUARD
#define AUTO_0_GUARD

#include "../drivetrain.h"
#include "../lifter.h"
#include "../autonFramework.h"

class Auto0: public Autonomous{
public:
  Auto0(Drivetrain *drive, Lifter *lifter);
  void init();
  void Periodic(double time);
  ~Auto0();
private:
  Drivetrain *drivetrain;
  Lifter *lifter;
};

#endif //AUTO_0_GUARD
