#ifndef AUTO_3_GUARD
#define AUTO_3_GUARD

#include "../drivetrain.h"
#include "../lifter.h"
#include "../autonFramework.h"

class Auto3: public Autonomous{
public:
  Auto3(Drivetrain *drive, Lifter *lifter);
  void init();
  void Periodic(double time);
  ~Auto3();
private:
  Drivetrain *drivetrain;
  Lifter *lifter;
  bool toteClose; //true if there is a tote within picking up range, false if not
  float toteCenterError; //center of the tote - the center of the image
};

#endif //AUTO_3_GUARD
