#include "Auto3.hpp"

Auto3::Auto3(Drivetrain *drive, Lifter *lifter)
{
  this->drivetrain=drive;
  this->lifter=lifter;
}
void Auto3::init()
{
  drivetrain->Enable();
  lifter->Enable();
  std::cout << "Starting auton 3" << std::endl;
}
void Auto3::Periodic(double autoTime)
{
    toteClose = false; // fix this
    toteCenterError = 0; // fix this
    if (autoTime < .1) {
      lifter->setClaw(1,1);
    }
    else if (autoTime > .1 && autoTime < .25)
    {
      lifter->setClaw(0,0);
    }
    else if (autoTime > .25 && autoTime < 1.75)
    {
      if (!toteClose)
      {
        drivetrain->update((.5*toteCenterError),-1,0,.5);
        lifter->gotoTarget(5000);
      }
      else
      {
        drivetrain->update(0,0,0,0);
        lifter->setClaw(1,1);
        lifter->gotoTarget(100);
      }
    }
    else if (autoTime > 1.75 && autoTime < 2)
    {
      lifter->setClaw(0,0);
    }
    else if (autoTime > 2 && autoTime < 3)
    {
      if (!toteClose)
          {
            drivetrain->update((.5*toteCenterError),-1,0,.5);
            lifter->gotoTarget(5000);
          }
          else
          {
            drivetrain->update(0,0,0,0);
            lifter->setClaw(1,1);
            lifter->gotoTarget(100);
          }
    }
}
Auto3::~Auto3()
{

}
