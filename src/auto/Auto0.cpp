#include "Auto0.hpp"

Auto0::Auto0(Drivetrain *drive, Lifter *lifter)
{
  this->drivetrain=drive;
  this->lifter=lifter;
}
void Auto0::init()
{
  drivetrain->Enable();
  lifter->Enable();
  std::cout << "Starting auton 0" << std::endl;
}
void Auto0::Periodic(double autotime)
{
  if (autotime < 3.5)
  {
    drivetrain->update(0,-1,0,.75);
  }
  else drivetrain->update(0,0,0,0);
}
Auto0::~Auto0()
{

}
