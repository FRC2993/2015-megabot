/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PWMINPUT_H_
#define PWMINPUT_H_

#include <PIDSource.h>
#include <chrono>
#include <DigitalInput.h>
#include <RotoBuffer.hpp>

namespace PWMInputMisc {
struct TurnPoint {
  std::chrono::high_resolution_clock::time_point startPoint;
  double duration;
};
  struct InterruptParams {
    std::chrono::high_resolution_clock::time_point startPoint;
    double duration;
    RotoBuffer<PWMInputMisc::TurnPoint>* rb;
  };
  void InterruptHandlerRising(unsigned int, void *data);
  void InterruptHandlerFalling(unsigned int, void *data);
}

class PWMInput: public PIDSource
{
public:
  PWMInput(uint32_t channel, uint32_t channel2);
  double PIDGet();
  double getValue();
  virtual ~PWMInput();
protected:
  double durDelt(int a, int b);
  DigitalInput *inputRising;
  DigitalInput *inputFalling;
  PWMInputMisc::InterruptParams data;
  RotoBuffer<PWMInputMisc::TurnPoint> buf;
  RotoBuffer<double> deltaBuf;
};

#endif /* PWMINPUT_H_ */
