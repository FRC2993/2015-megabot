/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SRC_ROTOBUFFER_HPP_
#define SRC_ROTOBUFFER_HPP_

template <class T>
class RotoBuffer
{
public:
  RotoBuffer(int size_) : size(size_)
  {
    vals = (T*)calloc(size,sizeof(T));
    start=0;
  }
  void push(T v)
  {
    start++;
    if(start>=size)start-=size;
    vals[start] = v;
  }
  T at(int place)
  {
    int p = start+place;
    while (p<0)p+=size;
    while (p>=size)p-=size;
    return this->vals[p];
  }
  ~RotoBuffer()
  {
    free(vals);
  }
protected:
  int size;
  T* vals;
  int start;
};

#endif /* SRC_ROTOBUFFER_HPP_ */
