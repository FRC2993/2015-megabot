/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//comment out the #define if you don't want to do PID for the drive
// values as P, I, D, Imax
#define DO_PID 0.5175,0.002,0,0.0 //Ultimate Gain ~ 1.15

#include "drivetrain.h"
#include "PWMInput.h"


Drivetrain& Drivetrain::operator=(const Drivetrain&)
{
  return *this;
}

Drivetrain::Drivetrain():
  driveFR(20), // all drive can ids are 20-29
  driveRR(21),
  driveFL(22),
  driveRL(23)
{

#ifdef DO_PID
  driveFR.setInfo(DO_PID,0,1);
  driveFL.setInfo(DO_PID,2,3);
  driveRR.setInfo(DO_PID,4,5);
  driveRL.setInfo(DO_PID,6,7);
#endif

#define SETUP_TALON( name ) name .SetExpiration(.250); name .SetSafetyEnabled(true)
  SETUP_TALON(driveFR); // setup safety
  SETUP_TALON(driveFL);
  SETUP_TALON(driveRR);
  SETUP_TALON(driveRL);
}

void Drivetrain::Enable()
{
#define ENABLE_TALON( name ) name .EnableControl(); name .Set(0)
  ENABLE_TALON(driveFR);
  ENABLE_TALON(driveFL);
  ENABLE_TALON(driveRR);
  ENABLE_TALON(driveRL);

}

void Drivetrain::update(float DriveX, float DriveY, float DriveR, float Throttle)
{
  driveFR.Set(( DriveX - DriveR + DriveY )* (  Throttle) );
  driveRR.Set(( DriveX + DriveR - DriveY )* (- Throttle) );
  driveFL.Set(( DriveX - DriveR - DriveY )* (  Throttle) );
  driveRL.Set(( DriveX + DriveR + DriveY )* (- Throttle) );
}

Drivetrain::~Drivetrain()
{
}

