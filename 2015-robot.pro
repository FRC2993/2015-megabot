TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/PWMInput.cpp \
    src/Robot.cpp \
    src/drivetrain.cpp \
    src/lifter.cpp \
    src/auto/Auto1.cpp \
    src/autonframework.cpp \
    src/auto/Auto2.cpp \
    src/pidcontroller.cpp

HEADERS += \
    src/PWMInput.h \
    src/RotoBuffer.hpp \
    src/joystickdefs.hpp \
    src/drivetrain.h \
    src/lifter.h \
    src/auto/Auto1.hpp \
    src/autonframework.h \
    src/auto/Auto2.hpp \
    src/pidcontroller.h

